package com.oscarcode.CRUD.Controller;


import com.oscarcode.CRUD.Models.Person;
import com.oscarcode.CRUD.Service.ServicesPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class controller {

    @Autowired
    ServicesPerson servicesPerson;


    @GetMapping("/getAllPerson")
    public List<Person> getPerson(){
        return servicesPerson.getAllPerson();
    }

    @PostMapping("/addPerson")
    public List<Person> addPerson(@RequestBody Person person){
        return servicesPerson.addPerson(person);
    }

    //@PathVariable
}
