package com.oscarcode.CRUD.Service;

import com.oscarcode.CRUD.Models.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ServicesPerson {

    List<Person> listPerson = new ArrayList<>();

    public ServicesPerson(){

        Person person1 = new Person();
        person1.setName("Oscar");
        person1.setAge(22);

        listPerson.add(person1);
    }


    public List<Person> getAllPerson(){
        return listPerson;
    }

    public List<Person> addPerson(Person person){
        listPerson.add(person);
        return listPerson;
    }

}
